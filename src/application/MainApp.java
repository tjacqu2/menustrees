package application;
	
import java.io.IOException;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import view.WorldWindController;
import javafx.scene.Scene;

import javafx.scene.control.SplitPane;

import javafx.scene.layout.BorderPane;



public class MainApp extends Application {
	
	
	private Stage primaryStage;
	private BorderPane rootPane;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("WorldWind");
		
		initRootLayout();
		showWorldWind();
		
	}
	
	public void initRootLayout() {
		try {
			
			FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/root.fxml"));
            rootPane = (BorderPane) loader.load();
            Scene scene = new Scene(rootPane);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
            
			/*SplitPane root = new SplitPane();
			Scene scene = new Scene(root,400,400);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			StackPane sp1 = new StackPane();
			final Menu file = new Menu("File");
			MenuItem open = new MenuItem("Open");
			MenuItem save = new MenuItem("Save");
			MenuItem saveAs = new MenuItem("Save As");
			MenuItem cancel = new MenuItem("Cancel");
			file.getItems().addAll(open, save, saveAs, cancel);
			final Menu menu2 = new Menu("Menu2");
			MenuBar menuBar = new MenuBar();
			menuBar.getMenus().addAll(file, menu2);
			final TreeItem<String> players = new TreeItem<String>("Players");
			players.setExpanded(true);			
			TreeItem<String> item1 = new TreeItem<String>("Team1");
			TreeItem<String> item2 = new TreeItem<String>("Team2");
			//does not like addAll(), better if one at a time
			players.getChildren().add(item1);
			players.getChildren().add(item2);
			TreeView<String> treeView = new TreeView<String>(players);
			VBox vb1 = new VBox();
			vb1.getChildren().addAll(menuBar, treeView);
			sp1.getChildren().add(vb1);
			StackPane sp2 = new StackPane();
			//add Worldwind here
			root.getItems().addAll(sp1,sp2);
			
			primaryStage.setScene(scene);
			primaryStage.show();*/
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showWorldWind() {
		try {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/worldWind.fxml"));
        SplitPane worldWindPane = (SplitPane) loader.load();
        rootPane.setCenter(worldWindPane);
        
        // Give the controller access to the main app.
        WorldWindController controller = loader.getController();
        controller.setMainApp(this);
		
		} catch(IOException e) {
			e.printStackTrace();
			
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
