package view;

public interface MenuItemListener {
	public abstract void onAction(); 
}
