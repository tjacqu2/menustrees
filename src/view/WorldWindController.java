package view;

import java.net.URL;
import java.util.ResourceBundle;

import application.MainApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;


public class WorldWindController implements Initializable{
	
	@FXML
	public VBox vbox;
	//@FXML
	//public TreeView<String> treeView;
	//@FXML
	//public MenuBar menuBar;
	
	private MainApp mainApp;
	
	//EventHandler<MenuItem> me = PlayerScenarioListener.onAction();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		TreeItem<String> playersRoot = new TreeItem<String>("Players");
		playersRoot.setExpanded(true);			
		TreeItem<String> item1 = new TreeItem<String>("Team1");
		TreeItem<String> item2 = new TreeItem<String>("Team2");
		playersRoot.getChildren().add(item1);
		playersRoot.getChildren().add(item2);
		TreeView<String> treeView = new TreeView<String>(playersRoot);
		MenuBar menuBar = new MenuBar();
		//treeView.setRoot(playersRoot);
		
		//MenuItem to set listener on
		MenuItem newScenario = new MenuItem("New");
		
		final Menu menu1 = new Menu("File");
		final Menu menu2 = new Menu("Options");
		menu1.getItems().add(newScenario);
		menuBar.getMenus().addAll(menu1, menu2);
		vbox.getChildren().addAll(menuBar, treeView);
		
		
		//register listener on MenuItem
		newScenario.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {
				PlayerScenarioListener  p = new PlayerScenarioListener();
				p.onAction();
			}
		});
		
		
	}
	
	
	public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
	}
		
		
}
